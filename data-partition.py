import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
from pyspark.sql.functions import col,year,month,dayofmonth,to_date

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME' , 'catalog_database' , 'catalog_database_table' , 'target_data_path'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

print("catalog_database: ", args['catalog_database'])
print("catalog_database_table: ", args['catalog_database_table'])
print("target_data_path: ", args['target_data_path'])

## @type: DataSource
## @args: [database = args['catalog_database'], table_name = args['catalog_database_table'], transformation_ctx = "datasource0"]
## @return: datasource0
## @inputs: []
datasource0 = glueContext.create_dynamic_frame.from_catalog(database = args['catalog_database'] , table_name = args['catalog_database_table'] , transformation_ctx = "datasource0")


## Get Year , Month , Day Partition Columns
source_df = datasource0.toDF()
# print("  source_df ", source_df.show() )


# trasaction_date_df =  source_df.withColumn("transaction_date", source_df["pickup_datetime"].cast('date'))
# print("  trasaction_date_df ", trasaction_date_df.show() )


partition_df = source_df.withColumn("transaction_date", source_df["lpep_pickup_datetime"].cast('date'))\
                .withColumn("year", year(col("transaction_date")))\
                .withColumn("month", month(col("transaction_date")))\
                .withColumn("day", dayofmonth(col("transaction_date")))\
                .drop(col("transaction_date"))

partition_dynamic_frame = DynamicFrame.fromDF(partition_df , glueContext ,  "partition_dynamic_frame")

# Convert the DataFrame back to DynamicFrame

datasink4 = glueContext.write_dynamic_frame.from_options(frame = partition_dynamic_frame , connection_type = "s3", connection_options = {"path": args['target_data_path'] , "partitionKeys": ["year", "month", "trip_type"] }, format = "parquet", transformation_ctx = "datasink4")
job.commit()